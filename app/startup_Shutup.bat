
:: Run in silence mode
ECHO OFF
cls

:: Write a message
ECHO Waiting for start


:: You can switch to Ping to avoid any texts
:: PING -n 11 127.0.0.1>nul
TIMEOUT 10

:: Save vvvv path in vvvv variable (and strips from unnecessary characters)
ftype vvvv > vvvvPath.txt
set /p vvvv=<vvvvPath.txt
set vvvv=%vvvv:~6,-9%

:: You could set specific vvvv version to variable vvvv

:: Start vvvv and open _main
start "" "%vvvv%" /o "%CD%\patches\_root.v4p" /shutup